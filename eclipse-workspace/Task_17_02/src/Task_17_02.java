import java.util.Arrays;
import java.util.List;

public class Task_17_02 {

	public static void main(String[] args) {

		System.out.println("This program modify every element in list to Upper case, except the number length is 2 or less");

		List<String> names = Arrays.asList("aDs", "add", "dd", "eee");
		System.out.print("{");

		for (int i = 0; i < names.size(); i++) {

			if (names.get(i).length() > 2) {
				System.out.print("\"" + names.get(i).toUpperCase() + "\"");
			} else {
				System.out.print("\"" + names.get(i) + "\"");
			}

			if (i < names.size() - 1) {
				System.out.print(",");
			}

		}

		System.out.print("}");

	}

}

import java.util.ArrayList;
import java.util.HashMap;

public class HashMap_example {

	public static void main(String[] args) {
		
		HashMap<String, ArrayList<String>> map = new HashMap<>();
		
		
		ArrayList<String> gradesD = new ArrayList<>();
		gradesD.add("10");
		gradesD.add("8");
		gradesD.add("5");
		
		ArrayList<String> gradesE = new ArrayList<>();
		gradesE.add("6");
		gradesE.add("8");
		gradesE.add("5");
		
		ArrayList<String> gradesG = new ArrayList<>();
		gradesG.add("8");
		gradesG.add("8");
		gradesG.add("5");
		
		map.put("Edijs", gradesD);
		map.put("Zane", gradesE);
		map.put("Gusts", gradesG);
		
		
		
		
		System.out.println("map: "+map);
		

	}

}

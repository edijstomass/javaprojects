
public class Task_26 {

	public static void main(String[] args) {

		System.out.println("This program allows to combine Strings");

		combineStrings("a", "b", "c", "name5");

	}

	public static String combineStrings(String... names) { // Varargs sintakse

		String collectNames = "";

		for (int i = 0; i < names.length; i++) {
			collectNames += names[i];
			System.out.print(names[i]);
		}

		return collectNames;

	}

}

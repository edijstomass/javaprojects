import java.util.Scanner;

public class Board {

	
	private String[][] array2D = { { "|"," ","|"," ","|"," ","|"},	//01; 03; 05
								   { "|"," ","|"," ","|"," ","|"}, 	//11; 13; 15
								   { "|"," ","|"," ","|"," ","|"}};	//21; 23; 25
	
	
	
	
	
	
	
	
	public String[][] getArray2D() // Getter - retrieve Array2D variable (method)
	{
		return this.array2D;
	}
	
	
	
	public void displayBoard(String[][] array2D) // metode, kas izprinte array2D in Matrix
	{
		
		for (int i = 0; i < array2D.length; i++) 
		{
		    for (int j = 0; j < array2D[i].length; j++) 
		    {
		        System.out.print(array2D[i][j] + "");
		    }
		    
		    System.out.println();
		}
		
	}
	
	
	public String movementValue()	// returns X or O for move
	{
		Scanner scan = new Scanner(System.in);
		
		
		int check = 0;
		
		while (check < 5) {
			System.out.print("Enter 'X' or 'O': ");
			String input = scan.nextLine().toUpperCase();
			if(input.equals("X"))
			{
				check = 10;
				return "X";
			}else if(input.equals("O"))
			{
				check = 10;
				return "O";
			}
				
		}
		
		//scan.close();
		return "False input";
		
	}
	
	
	public boolean checkPossibleMovement(String[][] array2D, int[] num) // checks empty fields
	{
		
		if(array2D[num[0]][num[1]].equals(" "))		
		{
			return true;
		}
		else
		{
			return false;
		}
		//return false;
	}
	
	
	
	
	
	public int checkWinner(String[][] array2D) // check every line in board
	{
	
		if(array2D[0][1] == "X" && array2D[0][3] == "X" && array2D[0][5] == "X"){
			System.out.println("X is Winner!");
			return 1;
		}
		else if(array2D[1][1] == "X" && array2D[1][3] == "X" && array2D[1][5] == "X"){
			System.out.println("X is Winner!");return 1;
		}
		else if(array2D[2][1] == "X" && array2D[2][3] == "X" && array2D[2][5] == "X"){
			System.out.println("X is Winner!");return 1;
		}
		else if(array2D[0][1] == "X" && array2D[1][1] == "X" && array2D[2][1] == "X"){
			System.out.println("X is Winner!");return 1;
		}
		else if(array2D[0][3] == "X" && array2D[1][3] == "X" && array2D[2][3] == "X"){
			System.out.println("X is Winner!");return 1;
		}
		else if(array2D[0][5] == "X" && array2D[1][5] == "X" && array2D[2][5] == "X"){
			System.out.println("X is Winner!");return 1;
		}
		else if(array2D[0][1] == "X" && array2D[1][3] == "X" && array2D[2][5] == "X"){
			System.out.println("X is Winner!");return 1;
		}
		else if(array2D[0][5] == "X" && array2D[1][3] == "X" && array2D[2][1] == "X"){
			System.out.println("X is Winner!");return 1;
		}	
			
		
			if(array2D[0][1] == "O" && array2D[0][3] == "O" && array2D[0][5] == "O"){
				System.out.println("O is Winner!");return 2;
			}
			else if(array2D[1][1] == "O" && array2D[1][3] == "O" && array2D[1][5] == "O"){
				System.out.println("O is Winner!");return 2;
			}
			else if(array2D[2][1] == "O" && array2D[2][3] == "O" && array2D[2][5] == "O"){
				System.out.println("O is Winner!");return 2;
			}
			else if(array2D[0][1] == "O" && array2D[1][1] == "O" && array2D[2][1] == "O"){
				System.out.println("O is Winner!");return 2;
			}
			else if(array2D[0][3] == "O" && array2D[1][3] == "O" && array2D[2][3] == "O"){
				System.out.println("O is Winner!");return 2;
			}
			else if(array2D[0][5] == "O" && array2D[1][5] == "O" && array2D[2][5] == "O"){
				System.out.println("O is Winner!");return 2;
			}
			else if(array2D[0][1] == "O" && array2D[1][3] == "O" && array2D[2][5] == "O"){
				System.out.println("O is Winner!");return 2;
			}
			else if(array2D[0][5] == "O" && array2D[1][3] == "O" && array2D[2][1] == "O"){
				System.out.println("O is Winner!");return 2;
			}
		
			return 0;
	}
	
	
	public void checkDraw(String[][] array2D)
	{
		if(array2D[0][1] != "" && array2D[0][3] != "" && array2D[0][5] != "" &&
		   array2D[1][1] != "" && array2D[1][3] != "" && array2D[1][5] != "" &&
		   array2D[2][1] != "" && array2D[2][3] != "" && array2D[2][5] != "")		
		{
			System.out.println("It's Draw!");
		}

	}
	
	
	
	public void newGame(String[][] array2D)
	{
		array2D = this.array2D;
	}


	

	
}

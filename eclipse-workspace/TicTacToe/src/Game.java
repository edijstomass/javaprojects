
public class Game extends Board  {

	private String playerOne;
	private String playerTwo;
	
	
	public Game() {
		
		this.playerOne = "Edijs";
		this.playerTwo = "Computer";
	}
	
	public Game(String playerOne, String playerTwo) {
		
		this.playerOne = playerOne;
		this.playerTwo = playerTwo;
	}
	
	
	
	public void gameStatus(String[][] array2D)
	{
		if(checkWinner(array2D) == 1)
		{
			System.out.println("Winner is: "+ this.playerOne);
		}
		else if(checkWinner(array2D) == 2)
		{
			System.out.println("Winner is: " + this.playerTwo);
		}
		else
		{
			System.out.println("Draw");
		}
		
	
	}
	
	
	public void gamePlay(String[][] array2D)
	{
		displayBoard(array2D);
		
		System.out.println("Enter your next move");
		
	}
	
	
	
}


public enum GameStatus {

	Player_one_Wins,
	
	Player_two_Wins,
	
	Draw,
	
	The_game_is_Active;
	
}

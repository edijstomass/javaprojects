import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task_19 {

	public static void main(String[] args) {

		// If String element lenght is 4, than program will add '****' before element

		List<String> names = Arrays.asList("this", "is", "lots", "of", "fun", "for", "every", "Java", "programmer");

		List<String> names2 = new ArrayList<String>(Arrays.asList());

		for (int i = 0; i < names.size(); i++) {

			if (names.get(i).length() == 4) {
				names2.add("****");
				names2.add(names.get(i));
			} else {
				names2.add(names.get(i));
			}

		}

		for (int i = 0; i < names2.size(); i++) {

			System.out.print("\"" + names2.get(i) + "\"");
			if (i < names2.size() - 1)
				System.out.print(",");

		}

	}

}

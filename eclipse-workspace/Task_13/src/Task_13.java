import java.lang.Math;

public class Task_13 {

	public static void main(String[] args) {

		System.out.println("This program raise every number in the array to the power of 3 ");
		
		int[] numbers = new int[] { 1, 35, 6, 7, 9 };
		int i = 1;
		System.out.print("[");

		for (int number : numbers) {
			number = (int) Math.pow(number, 3);
			System.out.print(number);

			if (i < numbers.length) {
				System.out.print(",");
				i++;
			}
		}
		System.out.print("]");

	}

}

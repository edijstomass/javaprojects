
public class ForEach {

	public static void main(String[] args) {
		
		int[] array = new int[] {5, 12, 0, 12, 55, 4, 23, -4, 44};
		
		for(int number : array)
		{
			System.out.println(number);
		}
		
		
		int biggest = MaxNumber(array);
		System.out.println("The Max number is: "+biggest);
		
		
		
		
		

	}
	
	static int MaxNumber(int array[])
	{
		int max = array[0];
		
		for(int number : array)
		{
			if(number>max)
			{
				max = number;
			}
		}
		return max;
	}
	
	

}

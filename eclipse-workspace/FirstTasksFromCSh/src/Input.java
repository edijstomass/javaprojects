import java.util.Scanner;

public class Input {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter your name");
		String my_name = scan.nextLine();
		
		System.out.println("My name is: " +my_name);
		
		
		scan.close();

	}

}

import java.util.Scanner;

public class Task_7 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter type of X-mas tree");
		String tree = scan.nextLine().toLowerCase();
		
		int treeHight = 66;
		int branchDiameter = 120;
		
		if(treeHight >=50 && treeHight <100 && branchDiameter >= 100 && branchDiameter < 150 &&
				tree.charAt(0) != 's')
		{
			System.out.println("Room 1");
		}
		else if(treeHight >=100 && treeHight <150 && branchDiameter >= 150 && branchDiameter < 200 &&
				tree.charAt(0) != 's')
		{
			System.out.println("Room 2");
		}
		else if(treeHight >=150 && treeHight <200 && branchDiameter >= 200 && branchDiameter < 250 ||
				tree.charAt(0) == 's')
		{
			System.out.println("Fireplace room");
		}else {
			System.out.println("Burn that tree");
		}
		
		scan.close();
	}

}

import java.util.Scanner;

public class SubString {
	public static void main(String[] args) {
	
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter your name: ");
		String name = scan.nextLine();
		
		System.out.println("Enter your surname: ");
		String surname = scan.nextLine();
		
		System.out.println("Wellcome, "+name +" "+ surname+"!");
		
		name = name.substring(0, 1);
		System.out.println("Wellcome, "+name +"."+ surname+"!");
		
		// Var ari viena linija: System.out.println(name.substring(0,1);
		
		if(name == "E")
		{
			System.out.println("This is true");
		}
		
		//System.out.println(name);
		scan.close();
	}
	

}

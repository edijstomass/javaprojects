import java.util.Scanner;

public class WhileLoopAndTryCatch {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		
		while (true) {
			
			
			
			
			try {
				System.out.println("Enter a number");
				int number = scan.nextInt();
				break;
				
			} catch (Exception e) {
				
				System.out.println("It's not a number");
				scan.next();
				
			}

			
		}
		
		
		System.out.println("Programms end");
		
		
		
		scan.close();
		

	}

}

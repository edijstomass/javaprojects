import java.util.Scanner;

public class IfElseIf {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Speed limit was exceeded by: ");
		int speed = scan.nextInt();
		
		if(speed >= 10 && speed <= 20)
		{
			System.out.println("Penalty: 15,- euro");
		}
		else if(speed > 20 && speed <= 30)
		{
			System.out.println("Penalty: 30,- euro");
		}else if(speed > 30)
		{
			System.out.println("You busted your license");
		}else {
			System.out.println("Warning");
		}
		
		scan.close();
		
	}

}

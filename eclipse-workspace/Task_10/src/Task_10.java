import java.util.Random;
import java.util.Scanner;

public class Task_10 {

	public static void main(String[] args) {

		int number = new Random().nextInt(11);

		Scanner scan = new Scanner(System.in);

		System.out.print("Guess the number between 1 and 10: ");

		while (true) {
			int myGuess = isDigit(scan);
			if (number == myGuess) {
				System.out.println("Congratulation, you Win!");
				break;
			} else {
				System.out.println("Nop, try again!");
			}

		}

		scan.close();

	}

	public static int isDigit(Scanner scan) {

		do {
			int num;
			try {
				num = scan.nextInt();
				return num;
			} catch (Exception e) {
				System.out.println("You didn't entered a number");
				scan.next();
			}
		} while (true);

	}

}

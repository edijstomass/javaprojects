import java.util.Scanner;

public class Task_03 {

	public static void main(String[] args) {

		System.out.println("This program sum number1 and number2. If the sum is >= 10 and <=20, print 20");
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter number 1:");
		int x = isDigit(scan);

		System.out.println("Enter number 2:");
		int y = isDigit(scan);

		scan.close();
		int sum = x + y;

		if (sum >= 10 && sum <= 20) {
			System.out.println("20");
		} else {
			System.out.println(sum);
		}

	}

	public static int isDigit(Scanner scan) {

		do {
			int num;
			try {
				num = scan.nextInt();
				return num;
			} catch (Exception e) {
				System.out.println("You didn't entered a number");
				scan.next();
			}
		} while (true);

	}

}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task_18 {

	public static void main(String[] args) {

		System.out.println("This program connects two lists in one list and prints out all element values to the power of 3");
		
		List<Integer> numbers1 = Arrays.asList(2, 4, 7, 4, 8, 6);
		List<Integer> numbers2 = Arrays.asList(9, 1, 9, 15, 7, 3);

		
		List<Integer> numbers3 = new ArrayList<Integer>();
		numbers3.addAll(numbers1);
		numbers3.addAll(numbers2);

		
		for (int i = 0; i < numbers3.size(); i++) {
			System.out.print((int) Math.pow(numbers3.get(i), 3));

			if (i < numbers3.size() - 1) {
				System.out.print(",");
			}
		}

		
		
	}

}

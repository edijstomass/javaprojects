import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task_20 {

	public static void main(String[] args) {

		System.out.println("This program prints out only unique numbers");
		
		List<Integer> numbers1 = Arrays.asList(1, 5, 4, 2, 3, 1, 5, 2, 3, 1);
		List<Integer> numbers2 = new ArrayList<Integer>();

		
		for (int i = 0; i < numbers1.size(); i++) {

			if (!numbers2.contains(numbers1.get(i))) {
				numbers2.add(numbers1.get(i));
			}

		}

		for (int i = 0; i < numbers2.size(); i++) {

			System.out.print(numbers2.get(i));
			if (i < numbers2.size() - 1) {
				System.out.print(",");
			}
		}

		
		
	}
}
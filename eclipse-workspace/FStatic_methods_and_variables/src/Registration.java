
public class Registration {

	public String username;   // create every time new variable after next user is created
	public static int usernameCounter = 0; // static continue to count, doens't create a new var
	
	
	public Registration(String nameCTR)
	{
		username = nameCTR;
		usernameCounter++;
		
	}
	
	
	public String putUserNameOnDataBase() // Return Method
	{
	return username;
	// puts username on some DataBase
	// also allows to print user1, user2 etc as string.
	}
	
	public static int getUsernameCount()  // Static method
	{
		return usernameCounter;
	}
	
	
	
	
}

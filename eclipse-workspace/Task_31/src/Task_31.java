import java.util.Scanner;

public class Task_31 {

	public static void main(String[] args) {
		
		// Scrable
		
		System.out.print("Enter a word: ");
		Scanner scan = new Scanner(System.in);
		String name = scan.nextLine().toUpperCase();
		scan.close();
		
		int points = 0;
		
		for (int i = 0; i < name.length(); i++) {
			

			if(name.charAt(i) == 'A' || name.charAt(i) == 'E' || name.charAt(i) == 'I' ||
			   name.charAt(i) == 'O' || name.charAt(i) == 'U' || name.charAt(i) == 'L' ||
			   name.charAt(i) == 'N' || name.charAt(i) == 'R' || name.charAt(i) == 'S' ||
			   name.charAt(i) == 'T') {
				points += 1;
			}else if(name.charAt(i) == 'D' || name.charAt(i) == 'G') {
				points +=2;
			}else if(name.charAt(i) == 'B' || name.charAt(i) == 'C' || name.charAt(i) == 'M' ||
					 name.charAt(i) == 'P') {
				points +=3;
			}else if(name.charAt(i) == 'F' || name.charAt(i) == 'H' || name.charAt(i) == 'V' ||
					 name.charAt(i) == 'W' || name.charAt(i) == 'Y' ) {
				points +=4;
			}else if(name.charAt(i) == 'k') {
				points +=5;
			}else if(name.charAt(i) == 'J' || name.charAt(i) == 'X') {
				points += 8;
			}else if(name.charAt(i) == 'Q' || name.charAt(i) == 'Z')
			{
				points +=10;
			}

		}
		
		System.out.print(name+"->"+points);
		
		
	}

}

//String onePoint = "AEIOULNRST";
//String twoPoints = "DG";
//String threePoints = "BCMP";
//String fourPoints = "FHVWY";
//String fivePoints = "K";
//String eightPoints = "JX";
//String tenPoints = "QZ";
//
//
//if(name.contains(onePoint))

import java.util.Random;
import java.util.Scanner;

public class SortArray {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter array size between 20 and 40: ");
		int size = scan.nextInt();
		scan.close();
		
		if(size <20 || size >40)
		{
			System.err.println("Invalid number");
			System.exit(0);
		}
		
		int[] array = new int[size];

		for (int i = 0; i < array.length; i++) {
			array[i] = new Random().nextInt(90) + 10;
			System.out.print(array[i]);
			
			if(i<array.length-1)
			{
				System.out.print(", ");
			}
		}
		
		
		System.out.println();
		
		
		int number1 = 0;
		int check = 1;
		
		while(check == 1)
		{
			
			for (int i = 0; i < array.length-1; i++)
			{
				
				if(array[i] > array[i+1]) // 55 > 89
				{
					check = 1;
					
					number1 = array[i];  
					array[i] = array[i+1];
					array[i+1]= number1;
					
					break;	
				}else
				{
					check = 0;
				}
	
			}	
		
		}
		
		
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]);
			
			if(i<array.length-1)
			{
				System.out.print(", ");
			}
			
		}
		
		
		
		
		
		
		
	}
	
	
//	public static void swap(int[] array)
//	{
//		int number1 =  array[0];  
//		array[0] = array[1];
//		array[0]= number1;
//	}
	
	

}

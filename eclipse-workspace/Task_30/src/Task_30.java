import java.util.Scanner;
import java.text.DecimalFormat;

public class Task_30 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		DecimalFormat df = new DecimalFormat("#.00");
		
		System.out.print("Number1 : ");
		double num1 = scan.nextDouble();
		
		System.out.print("Number2 : ");
		double num2 = scan.nextDouble();
		
		System.out.print("+-*/ : ");
		char operator = scan.next().charAt(0);
		scan.close();
		
		double result = Calculator(num1, operator, num2);

		System.out.println("Result: " + df.format(result));
		
	}
	
	
	public static double Calculator(double num1, char operator, double num2)
	{
		
		
		if(operator == '+'){	
			return num1+num2;
		}else if(operator == '-') {
			return num1-num2;
		}else if(operator == '*') {
			return num1*num2;
		}else if(operator == '/') {
			return num1/num2;
		}
		

		return 0;
	}
	
	
}

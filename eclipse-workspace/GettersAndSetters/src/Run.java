
public class Run {

	public static void main(String[] args) {
		
		SimpleGettersAndSetters obj = new SimpleGettersAndSetters();
		
		obj.setNumber(10);
		
		int num = obj.getNumber();
		
		System.out.println(num);
		

		int copyOfNum = obj.getNumber();
		copyOfNum = 5;						// sadi var darit tikai ar value type... ref type izmainis ari field value
		
		System.out.println("My interested results:");
		System.out.println("copyOfNum: "+copyOfNum+" And obj: "+ num);
		
	}

}

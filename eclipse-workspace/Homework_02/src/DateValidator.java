import java.util.Scanner;

public class DateValidator {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter a date(month day as digit): ");
		int date = scan.nextInt();
		System.out.print("Enter a month(as digit): ");
		int month = scan.nextInt();
		System.out.print("Enter a year(as digit): ");
		int year = scan.nextInt();
		scan.close();
		String[] months = {"january", "february", "march", "april", 
						    "may", "june", "july", "august",
							"september", "october", "november", "december"};
		
		
		System.out.println(checkDate(date, month, year, months));
		
	
	}
	
	public static String checkDate(int date, int month, int year, String[] months)
	{
		boolean checkLongYear;
		checkLongYear = year % 4 == 0;
		
		if(date <= 28 && month == 2 && checkLongYear == false)
		{
			return date + "." + months[month-1] + " " + year;
		}
		if(date <= 29 && month == 2 && checkLongYear == true)
		{
			return date + "." + months[month-1] + " " + year;
		}
		if(date <= 30 && month == 4 || month == 6 || month == 9 || month == 11)
		{
			return date + "." + months[month-1] + " " + year;
		}
		if(date <= 31 && month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
		{
			return date + "." + months[month-1] + " " + year;
		}
		
		return "You entered wrong date or month or year";
	}
	
	
	

}

import java.util.Scanner;

public class HelloUser {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter your name and surname: ");
		String nameSurname = scan.nextLine();
		scan.close();
		
		int cutNameSurname = nameSurname.indexOf(" ");
		
		String name = nameSurname.substring(0, cutNameSurname);
		String surname = nameSurname.substring(cutNameSurname+1);
		String nameToUpper = name.substring(0, 1).toUpperCase() + name.substring(1, name.length()).toLowerCase();
		String surnameToUpper = surname.substring(0).toUpperCase();
		
		
		System.out.println("'"+nameToUpper+"'");
		System.out.println("'"+surnameToUpper+"'");

	}

}


public class Task_25 {

	public static void main(String[] args) {
		
		
		int[] numbers1 = new int[] {1,2,3,4,5,6,7};
		int[] numbers2 = new int[] {9,7,4,1};
		int[] numbers3 = new int[] {1,5,3,65,3};
		
		elementOrder(numbers1);
		elementOrder(numbers2);
		elementOrder(numbers3);
		
		
	}
	
	
	
	public static void elementOrder(int[] numbers)
	{
		
		
		int increasing = 0;
		int decreasing = 0;
		
		for (int i = 0; i < numbers.length-1; i++) {   // lenght-1, jo pedejo elementu nevajag salidzinat
			
				if(numbers[i] <= numbers[i +1])
				{
				increasing++;
				}
				else if(numbers[i] >= numbers[i +1])
				{
				decreasing++;
				}
		}
			
		
			
			if(increasing == numbers.length-1)
			{
				System.out.println("Increasing");
			}
			else if(decreasing == numbers.length-1)
			{
				System.out.println("Decreasing");
			}
			else
			{
				System.out.println("Not increasing, not decreasing");
			}
			
			
			
	}
	

	
}

import java.util.Scanner;

public class Task_32 {

	public static void main(String[] args) {

		System.out.println("This program from input creates a acronym.");

		Scanner scan = new Scanner(System.in);
		String name = scan.nextLine();
		scan.close();

		String acronym = "";

		for (int i = 0; i < name.length(); i++) {

			if (name.charAt(i) == ' ') { 	// if space detected
				acronym += name.charAt(i + 1);  // add first letter to String
			}

		}

		String acronym2 = name.charAt(0) + acronym; // first word letter + others first letters
		System.out.println(acronym2.toUpperCase());

	}

}

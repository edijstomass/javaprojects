
public class Task_12 {

	public static void main(String[] args) {

		System.out.println("This is fizz-buzz Task. Just run it!");
		
		int[] numbers = new int[20];

		System.out.print("[");

		for (int i = 1; i < numbers.length; i++) {

			numbers[i] = i;

			if (numbers[i] % 5 == 0 && numbers[i] % 3 == 0) {
				System.out.print("\"fizzBuzz\"");
			} else if (numbers[i] % 3 == 0) {
				System.out.print("\"fizz\"");
			} else if (numbers[i] % 5 == 0) {
				System.out.print("\"buzz\"");
			} else {
				System.out.print("\"" + numbers[i] + "\"");
			}

			if (i < numbers.length - 1) {
				System.out.print(",");
			}

		}

		System.out.print("]");

	}

}

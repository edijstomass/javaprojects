
public class Task_33 {

	public static void main(String[] args) {

		// Beer song lyrics
		
		int bottles = 99;

		while (bottles > 1) {
			System.out.println(bottles + " bottles beer on the wall, " + bottles + " bottles of beer.");
			bottles--;
			System.out.println("Take one down and pass it around, " + bottles + " bottles of beer on the wall.\n");

		}

		System.out.println(bottles + " bottles beer on the wall, " + bottles + " bottles of beer.");
		bottles--;
		System.out.println("Take one down and pass it around, " + bottles + " bottle of beer on the wall.\n");

		System.out.println("No more bottles of beer on the wall, no more bottles of beer.\n"
							+ "Go to the store and buy some more, 99 bottles of beer on the wall");

	}

}

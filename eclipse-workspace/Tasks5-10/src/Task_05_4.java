import java.util.Scanner;

public class Task_05_4 {

	public static void main(String[] args) {
		
		System.out.println("This program combine two words : shortest+longest+shortest");
		System.out.println("If lenght is equal, first word is longest\n");

		Scanner scan = new Scanner(System.in);

		System.out.print("Enter first word: ");
		String word1 = scan.nextLine();

		System.out.print("Enter second word: ");
		String word2 = scan.nextLine();

		scan.close();

		if (word1.length() > word2.length()) {
			System.out.println(word2 + word1 + word2);
		} else if (word2.length() > word1.length()) {
			System.out.println(word1 + word2 + word1);
		} else {
			System.out.println(word1 + word2 + word1);
		}

	}

}

import java.util.Scanner;

public class Task_09_Last {

	public static void main(String[] args) {

		System.out.println("This program prints out entered number multiplication table");
		Scanner scan = new Scanner(System.in);
		

		System.out.print("Enter the number: ");
		int number = Task_05_3.isDigit(scan);
		scan.close();

		
		for (int i = 0; i <= number; i++) {
			
				int sum = i * number;
				System.out.println(number + " X " + i + " = " + sum);

		}
		

	}

}

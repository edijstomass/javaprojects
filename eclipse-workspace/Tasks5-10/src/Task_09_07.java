import java.util.Scanner;

public class Task_09_07 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Prints out one character and shows thus sound type");
		System.out.print("Enter character: ");

		String word = scan.nextLine().substring(0, 1);
		word = word.toLowerCase();
		scan.close();


		String patskanis = "a�e�i�u�";
		String l�dzskanis = "bc�dfg�hjk�l�mn�rs�tvz�";
		String divskanis = "o";

		if (patskanis.contains(word)) {
			System.out.println(word + " ir patskanis");
		} else if (l�dzskanis.contains(word)) {
			System.out.println(word + " ir l�dzskanis");
		} else if (divskanis.contains(word)) {
			System.out.println(word + " ir divskanis");
		} else {
			System.out.println("Wrong input");
		}

	}

}

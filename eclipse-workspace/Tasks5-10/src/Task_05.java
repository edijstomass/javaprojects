import java.util.Scanner;

public class Task_05 {

	public static void main(String[] args) {

		System.out.println("This program sum only different numbers.\n");
		
		Scanner scan = new Scanner(System.in);

		System.out.print("Enter number 1: ");
		int x = Task_05_3.isDigit(scan);

		System.out.print("Enter number 2: ");
		int y = Task_05_3.isDigit(scan);

		System.out.print("Enter number 3: ");
		int z = Task_05_3.isDigit(scan);

		scan.close();
		int sum = x + y + z;

		if (x == z && x == y && y == z) {
			System.out.println("0");
		} else if (x == y) {
			System.out.println(z);
		} else if (x == z) {
			System.out.println(y);
		} else if (y == z) {
			System.out.println(x);
		} else {
			System.out.println("Sum is: " + sum);
		}

	}

}

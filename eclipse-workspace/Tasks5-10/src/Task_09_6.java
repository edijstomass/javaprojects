import java.util.Scanner;

public class Task_09_6 {

	public static void main(String[] args) {

		String[] months = new String[] { "January", "February", "March", "April", "May", "June", "July", "August",
										 "September", "October", "November", "December" };

		Scanner scan = new Scanner(System.in);

		System.out.print("Enter number of month: ");
		int monthNumber = Task_05_3.isDigit(scan);
		scan.close();

		try {

			System.out.print(months[monthNumber - 1]);

		} catch (Exception e) {

			System.out.print("You entered wrong number! Must enter 1 - 12");
		}

	}

}
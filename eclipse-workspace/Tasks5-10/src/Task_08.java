import java.util.Scanner;

public class Task_08 {

	public static void main(String[] args) {

		System.out.println("If entered number is natural: print:  0,1,2,3...(enter number) ");
		System.out.println("If entered number is negative: print: (enter number)....-5,-4,-3,-2,-1,0 \n");

		Scanner scan = new Scanner(System.in);
		System.out.print("Enter number: ");

		int number = Task_05_3.isDigit(scan);
		scan.close();

		int i = 0;

		while (i <= number) {
			System.out.print(i);
			i++;
			if (i <= number) {
				System.out.print(",");
			}
		}

		i = 0;

		while (i >= number) {
			System.out.print(number);
			number++;
			if (i >= number) {
				System.out.print(",");
			}
		}

	}

}

import java.util.Scanner;

public class Task_09 {

	public static void main(String[] args) {

		System.out.println("Izdrukas visus ciparus, kuri dal�s ar 2 un 3, bet ne ar 5");
		Scanner scan = new Scanner(System.in);

		System.out.print("Enter number one: ");
		int number = Task_05_3.isDigit(scan);

		System.out.print("Enter number two: ");
		int max = Task_05_3.isDigit(scan);

		scan.close();

		int counter = 0;

		while (number <= max) {

			if ((number % 2 == 0) && (number % 3 == 0) && (number % 5 != 0)) {

				if (counter != 0) {
					System.out.print(", ");
				}

				System.out.print(number);
				counter++;
			}

			number++;
		}

		if (counter == 0) {
			System.out.println("No result");
		}

	}

}

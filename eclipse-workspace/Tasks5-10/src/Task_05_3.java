import java.util.Scanner;

public class Task_05_3 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("This program prints true, if entered number is >= 50 and <100");
		System.out.print("Enter number : ");
		
		int x = isDigit(scan);
		scan.close();

		boolean num = (x >= 50 && x < 100);
		System.out.println(num);

		
	}

	
	
	public static int isDigit(Scanner scan) {

		do {
			int num;
			try {
				num = scan.nextInt();
				return num;
			} catch (Exception e) {
				System.out.println("You didn't entered a number");
				scan.next();
			}
		} while (true);

	}

}


public interface Vehicle {

	
	String getModel();
	
	String getColor();

	int getWheels();
	
	boolean isStreetLegal();
	
	void drive(int distance); // metode
	
	int getFuel(); // metode
	
		

	
}

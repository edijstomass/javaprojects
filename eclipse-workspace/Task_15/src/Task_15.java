
public class Task_15 {

	public static void main(String[] args) {

		System.out.println("This program prints unique array element dublicates!");
		
		String[] names = new String[] { "ABC", "CDE", "FGA", "CDE", "GAE", "ABC", "ABO" };

		for (int i = 0; i < names.length; i++) {
			for (int j = 0; j < names.length; j++) {
				if (names[i] == names[j] && i != j) {
					names[i] = "";
				}
			}
		}

		int counter = 0;

		for (String name : names) {
			if (!name.equals("")) {
				counter++;
			}
		}

		String[] uniqueNames = new String[counter];
		int j = 0;

		for (int i = 0; i < names.length; i++) {
			if (!names[i].equals("")) {
				uniqueNames[j] = names[i];
				j++;
			}
		}

		j = 0;

		for (String name : uniqueNames) {
			j++;
			System.out.print("\"" + name + "\"");
			if (j < uniqueNames.length) {
				System.out.print(",");
			}

		}

	}

}
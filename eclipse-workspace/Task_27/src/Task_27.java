
public class Task_27 {

	public static void main(String[] args) {

		System.out.println("This program returns reversed array");
		
		reverseArray("AB", "BA", "CA", "DE");
		System.out.println();
		reverseArray(1, 2, 3, 4);

	}

	public static void reverseArray(String... names) {
		int j = names.length - 1; // -1 jo lenght ir 4

		for (int i = 0; i < names.length; i++) {
			System.out.print("\"" + names[j] + "\"");
			j--;

			if (i < names.length - 1) {
				System.out.print(",");
			}

		}

	}

	public static void reverseArray(int... numbers) {

		int j = numbers.length - 1; // -1 jo lenght ir 4

		for (int i = 0; i < numbers.length; i++) {
			System.out.print("\"" + numbers[j] + "\"");
			j--;

			if (i < numbers.length - 1) {
				System.out.print(",");
			}

		}

		
		
	}

}


public class Rectangle extends Shape {

	// FIELDS

	protected int lenght = 0;
	protected int width = 0;

	// CONSTRUCTORS

	public Rectangle() {

	}

	public Rectangle(int leng, int wid) {
		this.lenght = leng;
		this.width = wid;
	}

	// METHODS

	public boolean isSquare() // metode - vai ir kvadrats
	{
		return lenght == width;
	}

	public int area() // metode - laukumam
	{
		return lenght * width;
	}

	public int perimeter() // metode - perimetram
	{
		return 2 * (lenght + width);
	}
	
	//@Override
	public String toString() {
		
		return "Rectangles lenght: " + lenght + " and width: " + width
				+"\nSquare -> "+ isSquare() 
				+ " \nArea -> " + area() 
				+ "\nPerimeter ->  " + perimeter()
				+ "\nColor ->  " + color;
	}
	
	


}

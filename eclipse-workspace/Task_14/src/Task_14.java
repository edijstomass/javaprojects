
public class Task_14 {

	public static void main(String[] args) {
		
		System.out.println("This program moves every '0' to the array beggining");

		int[] numbers = new int[] { 1, 35, 0, 7, 0, 0, 5, 4, 0 };


		System.out.print("[");
		
		int j = 0;
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] == 0) {
			int	checkNull = numbers[i];
			int checkOther = numbers[j];
				numbers[i] = checkOther;
				numbers[j] = checkNull;
				j++;
			}
		}

		j = 1;
		for (int number : numbers) {
			System.out.print(number);
			if (j < numbers.length) {
				System.out.print(",");
			}
			j++;
		}

		System.out.print("]");

	}

}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Task_21 {

	public static void main(String[] args) {

		System.out.println("This program remove entered String from List");
		
		List<String> names = new ArrayList<>(Arrays.asList("ABC", "CDE", "EFG", "CDE", "ANC", "CDE"));

		System.out.print("Enter a String: ");
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		scan.close();

		for (int i = 0; i < names.size(); i++) {

			if (names.get(i).equals(input)) {
				names.remove(input);
			}

		}

		for (int i = 0; i < names.size(); i++) {

			System.out.print("\"" + names.get(i) + "\"");
			if (i < names.size() - 1) {
				System.out.print(",");
			}

		}

		
		
	}

}

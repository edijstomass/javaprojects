import java.util.Scanner;

public class Task_24 {

	public static void main(String[] args) {

		
		System.out.println("Password length must be atleast 10 char");
		System.out.println("Only alphabetic letters allowed");
		System.out.println("Must include atleast 2 digits");
		System.out.print("Enter the password: ");
		
		Scanner scan = new Scanner(System.in);
		String password = scan.nextLine();
		scan.close();

		// tikai burti un cipari

		int onlyLetterAndDigit = 0;
		for (int i = 0; i < password.length(); i++) {

			if (Character.isDigit(password.charAt(i)) || Character.isAlphabetic(password.charAt(i))) {
				onlyLetterAndDigit++;
			}

		}

		
		// satur vismaz 2 ciparus
		int numberCount = 0;
		for (int i = 0; i < password.length(); i++) {

			if (Character.isDigit(password.charAt(i))) {
				numberCount++;
			}

		}

		
		if (password.length() >= 10 && numberCount >= 2 && onlyLetterAndDigit != 0) {
			System.out.println("Your password was created successfully!");
		} else {
			System.out.println("Try again!");
		}

		
		
	}

}


import java.text.DecimalFormat;

public class Task_16 {

	public static void main(String[] args) {
		DecimalFormat df2 = new DecimalFormat("#.00");

		double studentGrades[][] = { { 55, 43, 71 },   // Student one Grades
									 { 45, 59, 44 } }; // Student two Grades
													   // This program also allows to add new students and more grades
		double test = 0;

		for (int i = 0; i < studentGrades.length; i++) {
			test = 0;
			for (int j = 0; j < studentGrades[i].length; j++) {
				test += studentGrades[i][j];
			}

			test = test / 3;
			System.out.println("Average grades for student" + (i + 1) + ": " + df2.format(test));

		}

	}

}
//System.out.print(studentGrades[i][j] + " ");
//System.out.print(i + "  " + j);